package info.cybilltek.www.one.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import info.cybilltek.www.one.models.Sample;
import info.cybilltek.www.one.services.SampleService;

@RestController
@RequestMapping("/api/v1")
public class SampleController {

	@Autowired
	private SampleService sampleService;

	@GetMapping("/")
	public RepresentationModel<Sample> root() {
		RepresentationModel<Sample> rootResource = new RepresentationModel<Sample>();
		rootResource.add(
				WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(SampleController.class).root()).withSelfRel(), //
				WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(SampleController.class).getSamples())
						.withRel("samples"));

		return rootResource;
	}

	@GetMapping(value = "/samples", produces = { "application/hal+json" })
	public ResponseEntity<List<Sample>> getSamples() {
		List<Sample> samples = sampleService.getSamples();
		return new ResponseEntity<>(samples, HttpStatus.OK);
	}

	@GetMapping(value = "/sample/{id}", produces = { "application/hal+json" })
	public ResponseEntity<Sample> getSampleById(@PathVariable(value = "id") String id,
			HttpServletRequest httpServletRequest) throws ResourceNotFoundException {
		Sample sample = sampleService.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Sample not found for this id :: " + id));
		sample.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(SampleController.class).getSamples())
				.withSelfRel());
		return ResponseEntity.ok().body(sample);
	}

	@PostMapping("/sample")
	public ResponseEntity<Sample> saveSample(@RequestBody Sample sample) {
		Sample s = sampleService.saveSample(sample);
		return new ResponseEntity<>(s, HttpStatus.CREATED);
	}

	@PutMapping("/sample")
	public ResponseEntity<Sample> updateSample(@RequestBody Sample sample) {
		Sample s = sampleService.updateSample(sample);
		return new ResponseEntity<>(s, HttpStatus.CREATED);
	}

	@DeleteMapping("/sample")
	public ResponseEntity<String> deleteSample(@RequestParam(name = "id") String id) {
		String message = sampleService.getSample(id);
		return new ResponseEntity<>(message, HttpStatus.OK);
	}

}
