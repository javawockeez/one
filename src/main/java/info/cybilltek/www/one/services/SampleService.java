package info.cybilltek.www.one.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import info.cybilltek.www.one.models.Sample;
import info.cybilltek.www.one.repositories.SampleRepository;

@Service
public class SampleService {

	@Autowired
	private SampleRepository sampleRepository;

	public List<Sample> getSamples() {
		return sampleRepository.findAll();
	}

	public Optional<Sample> findById(String id) {
		return sampleRepository.findById(id);
	}

	public Sample saveSample(Sample sample) {
		return sampleRepository.save(sample);
	}

	public Sample updateSample(Sample sample) {
		return sampleRepository.save(sample);
	}

	public String getSample(String id) {
		sampleRepository.deleteById(id);
		return "Sample with id: " + id + " deleted successfully";
	}

}
