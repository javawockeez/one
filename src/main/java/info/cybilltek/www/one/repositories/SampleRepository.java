package info.cybilltek.www.one.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import info.cybilltek.www.one.models.Sample;

//@RepositoryRestResource(collectionResourceRel = "sample", path = "sample")
public interface SampleRepository extends MongoRepository<Sample, String> {

}
